# Diagnostique A Tiempo
## Front-end de la aplicación
### Realizado por Luis
#### Tareas generales
 - Templates de HTML [x]
 - Pasarlos a ReactJS [x]
 - Funciones en JavaScript para los filtrados [x]
 - Funciones para muestra de info [x]
## Conexiones a Backend/SendGrid
 - Axios para el endpoint en Python [x]

## Módulos a aplicar:
#### Nota: el orden no implica importancia ni forma en que se agregarán, sólo es el listado. Para relevancia de tareas, consultar el calendario en Google Drive o incluirlo como sección en este Readme
1. Navegación: react-router [x]
  - `<Router>`
  - `<Route path="/" exact component={componente} />`
  - `<Link to={path} />` --> No pueden estar afuera de un <em>Router</em>. EJ: **BrowserRouter**
##### Entre Luis y Emiliano, pero Luis las debe entender (la lógica es como require php pero en un archivo base).
1. Definición de pantallas [x]
  - Definición información por pantalla [x]
  - Formularios por pantalla [x]
  - Definición de rutas de la aplicación [x]
  - HTML templates [x]
  - Funcionalidades en ReactJS [x]
2. Establecimiento de archivos JSON.
  ### Opción para no tener muchas tablas o de otra manera, archivos más fácilmente controlables/editables.
  - Una sola llamada
  - Es manejarlos como arreglo de objetos.
  - Se le solicita así al cliente o al menos se estandariza cómo se pide
  - Esto sólo será para cosas que no se cambiarán tanto.
3. Manejo del <em>State</em> de la app con Context [x]
4. Login/Logout: Firebase [x]
5. Conexión a Firebase Database [x]
6. Conexión a Firebase Cloud Storage [x]
7. Pago a PayPal [x]
 - Envío del pago []
 - Recepción de realización o no realización del pago []

## Está ligada con:
### DAT - backend: conexiones a servidor/base de datos/Santander/APIs sólo se realizarán, el backend manejará todo.

## Dependencias instaladas por terminal:
1. yarn add react-router-dom - 5.2.0
2. yarn add axios - 0.19.2
3. yarn add firebase - 7.14.5
4. yarn add history - 4.10.1
5. yarn add signature_pad - 3.0.0-beta.3

## Dependencias por cdn/ script externo en el index.html
1. Firebase [x]
2. Sweet Alert 2.9 [x]


## Apoyos encontrados:

### PDF
- https://medium.com/yellowcode/download-api-files-with-react-fetch-393e4dae0d9e