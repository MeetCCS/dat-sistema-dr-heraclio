import React from 'react';

const AnuncioPositivoNegativo = ({ mensaje, comprobacion }) => {
  return mensaje === comprobacion ?
    <p className="anuncioNegativo">{mensaje}</p>
    : <p className="anuncioPositivo">{mensaje}</p>
};

export default AnuncioPositivoNegativo;