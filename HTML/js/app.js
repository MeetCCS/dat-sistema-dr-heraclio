'use strict'

function centrarInput(e) {
   let input = document.getElementById(e.target.id);
   if(input.value) {
      if(input === inputPagoFirma) {
        btnPagoFirma.disabled = false;
      }
   } else{
      input.classList.remove("text-center");
      btnPagoFirma.disabled = true;
   }
};

/* Función reloj para saludo */
function mostrarSaludo() {
  const hora = new Date().getHours();

  let texto = 'Buenas noches';
  let imagen = 'images/noches.svg';
  let fondo = 'noches';


   if(hora >= 6 && hora < 15) {
      texto = "Buenos días";
      imagen = "images/dias.svg";
   }

   if(hora >= 15 && hora < 20) {
      texto = "Buenas tardes";
      imagen = "images/tardes.svg";
   }

  if (document.getElementById('tiempo')) {
    document.getElementById('tiempo').src = imagen;
    document.getElementById('saludo').innerHTML = texto;
  }
  document.body.classList.add(fondo);

}

/* Función para cambiar de formulario */
let formDoctor = document.getElementById('formDoctor');
let formCliente = document.getElementById('formCliente');
let PacienteMedico = document.getElementById('PacienteMedico');

function ToggleForm() {
   if(formCliente.style.display === 'none') {
      formDoctor.style.opacity = 0;
      setTimeout(function() {
         formDoctor.style.display = "none";
         formCliente.style.display = "block";
         PacienteMedico.innerText = "¿Es médico?";
      }, 300);
      setTimeout(function() {
         formCliente.style.opacity = 1;
      }, 320);
   }else{
      formCliente.style.opacity = 0;
      setTimeout(function() {
         formCliente.style.display = "none";
         formDoctor.style.display = "block";
         PacienteMedico.innerText = "¿Es paciente?";
      }, 300);
      setTimeout(function() {
         formDoctor.style.opacity = 1;
      }, 320);
   }
};


///////////////////////////////
/*Sección de botones y campos*/
///////////////////////////////

/**
 * Función para habilitar campos dependiendo del form que lo solicite
 * Se concatena el string 'deshabilitado' con el número a editar que lo solicite.
 * Ej: deshabilitado1, deshabilitado2
 *
 * @param {number} idEdicion - id que recibes del form (pasado por el botón Editar)
 * @param {bool} ocultarEditar - si es falso, se activa la edición; si es verdadero, se desactivan los campos
 */
const ToggleCampos = (idEdicion, ocultarEditar) => {
	/**
	 * @const {*} campos - HTML Collection que poseen la calse desabilitadoVAR, donde VAR es un número.
	 */
	const campos = Array.from(document.getElementsByClassName(`deshabilitado${idEdicion}`));
	campos.forEach(campo => campo.disabled = ocultarEditar);
}

/**
 * Función para deshabilitar campos dependiendo del form que lo solicite
 * Ej: deshabilitado3, deshabilitado10, etc.
 *
 * @param {number} idEdicion - id que recibes del form (pasado por el botón Guardar)
 */
// desabilitarCampos = idEdicion => {
// 	const campos = document.getElementsByClassName(`deshabilitado${idEdicion}`);
// 	for(let i = 0; campos[i]; i++) {
//   	campos[i].disabled = true;
//   }
// }

/////////////////////////////////////////
/*Mmostrar campos para delimitar fechas*/
/////////////////////////////////////////

const MostrarRangoFecha = document.getElementById("tipoFiltro");
const Fechas = document.getElementById("Fechas");

function MostrarFechas() {
	if (MostrarRangoFecha.value === 'Fecha') {
		Fechas.classList.remove('cajaCerrada');
		Fechas.classList.add('cajaAbierta');
	} else {
		Fechas.classList.remove('cajaAbierta');
		Fechas.classList.add('cajaCerrada');
	}
}

//////////////////////////////////////
/*Habilitar campo de agregar estudio*/
//////////////////////////////////////

const SelectEstudioCotizacion = document.getElementById("SelectEstudioCoti");
const InputAgregarEstudio = document.getElementById("InputAgregarEstudio");

function habilitarCampo() {
	if(SelectEstudioCotizacion.value === 'Otro') {
		InputAgregarEstudio.disabled = false;
		InputAgregarEstudio.style.display = 'flex';
	}else{
		InputAgregarEstudio.disabled = true;
		InputAgregarEstudio.value = '';
		InputAgregarEstudio.style.display = 'none';
	}
}


function Mostrar() {

}
