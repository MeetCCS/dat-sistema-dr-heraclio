/**
 * Cuando se envía un submit
 *
 * @param {*} event - el evento que recibe del submit del form
 */
onFormSubmit = event => {
  event.preventDefault();
	console.log('El form fue enviado');
}