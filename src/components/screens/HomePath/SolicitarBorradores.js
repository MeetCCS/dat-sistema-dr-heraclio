import React, { useContext, useEffect, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import history from '../../Navigation';

import { Contexto as EstudiosContext } from '../../context/EstudiosContext';

import { RestContainer } from '../../modules/RestContainer';
import { Estudio } from '../screenNames';

const SolicitarBorradores = () => {
  const { state: { listadoBorradoresDoc } } = useContext(EstudiosContext);

  const [borradoresFiltrados, setDoctoresFiltrados] = useState([]);
  const [nomPaciente, setNomPaciente] = useState('');

  useEffect(() => {
    const nuevaLista = listadoBorradoresDoc.filter(estudio => {
      return estudio.nombrePaciente.toUpperCase().includes(nomPaciente.toUpperCase()) || estudio.apellidoPaciente.toUpperCase().includes(nomPaciente.toUpperCase());
    });
    setDoctoresFiltrados(nuevaLista);
  }, [listadoBorradoresDoc, nomPaciente]);

  if (borradoresFiltrados.length < 1) {
    return (
      <RestContainer>
        <div id="Seleccion" className="col-sm-12 col-md-12 col-lg-8">
          <h3 onClick={() => history.push('/home')} className="mb-3"><i className="fas fa-arrow-circle-left"></i> Buscar Solicitudes Inconclusas</h3>
          <div className="card mt-3 tarjetas">
            <div className="card-header">
              <h4>Resultados: {borradoresFiltrados.length} borradores</h4>
            </div>
            <div className="card-body">
              <ul className="list-group">
                <ul className="list-group">
                  <div className="list-group-item list-group-item-action">No hay doctores registrados en el sistema.</div>
                  <Redirect to={`/${Estudio}`} />
                </ul>
              </ul>
            </div>
          </div>
        </div>
      </RestContainer>
    );
  }

  return (
    <RestContainer>
      <div id="Seleccion" className="col-sm-12 col-md-12 col-lg-8">
        <h3 onClick={() => history.push('/home')} className="mb-3"><i className="fas fa-arrow-circle-left"></i> Buscar Solicitudes Inconclusas</h3>
        <div className="card mt-3 tarjetas">
          <div className="card-header">
            <h4>Resultados: {borradoresFiltrados.length} borradores</h4>
          </div>
          <div className="card-body">
            <ul className="list-group">
              <form className="mb-3">
                <div className="form-row">
                  <div className="col-12">
                    <input className="form-control mr-sm-2" type="search" placeholder="Buscar por nombre de paciente" aria-label="Buscar"
                      onChange={e => setNomPaciente(e.target.value)}
                      value={nomPaciente}
                    />
                  </div>
                </div>
              </form>

              <ul className="list-group">
                {
                  borradoresFiltrados.map(borrador => {
                    return <Link key={borrador.folio} to={`/${Estudio}`} className="list-group-item list-group-item-action">{borrador.nombrePaciente} {borrador.apellidoPaciente} - {borrador.folio}</Link>;
                  })
                }
              </ul>
            </ul>
          </div>
        </div>
      </div>
    </RestContainer>
  );
};

export default SolicitarBorradores;