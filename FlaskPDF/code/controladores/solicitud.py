from flask_restful import Resource, request

from sendEmail import sendEmailSolicitud, sendEmailFirma, sendEmailResListo
from regexEmail import check
from schemas.solicitud import SolicitudSchema, ListoOFirmaSchema
from pdf import generatePDF

solicitud_schema = SolicitudSchema()
listo_o_firma_schema = ListoOFirmaSchema()


class EnvioEmailSolicitud(Resource):
    '''
    Confirmación de la creación de estudio por correo.
    Datos requeridos:
    1) folio
    2) nombre doctor@
    3) correo doctor
    4 & 5) Archivos adjuntos
    '''

    @classmethod
    def post(self):
        sol_json = request.get_json()
        sol_data = solicitud_schema.load(sol_json)

        if sol_data['folio'] is None or sol_data['folio'] == '':
            return {'error': 'Fallo en envío de datos'}

        if check(sol_data['email']):
            # 1. Gneneración de PDF
            generatePDF(**sol_data)

            # 2. Envío de solicitud via email
            resultado = sendEmailSolicitud(sol_data['folio'], sol_data['nomDoc'], sol_data['email'], ['docHistopato'], sol_data['docOtros'])

            if resultado:
                return {'message': 'Envío exitoso'}
                # solicitud_schema.dump(resultado)
            return {'error': 'Fallo de envío de correo'}
        return {'error': 'No es un correo'}, 400


class EnvioEmailResultadoEmitido(Resource):
    '''
    Aviso de resultado emitido por correo.
    Datos requeridos:
    1) folio
    2) nombre doctor@
    3) correo doctor
    '''

    def post(self):
        sol_json = request.get_json()
        sol_data = listo_o_firma_schema.load(sol_json)
        
        if check(sol_data['correoDoc']):
            resultado = sendEmailResListo(**sol_data)
            if resultado:
                return {'message': 'Envío exitoso'}
            return {'error': 'Fallo de envío de correo'}
        return {'error': 'No es un correo'}, 400


class EnvioSolicitudAdminFirma(Resource):
    '''
    Aviso de resultado emitido por correo.
    Datos requeridos:
    1) folio
    2) nombre doctor@
    3) correo doctor
    '''

    def post(self):
        sol_json = request.get_json()
        sol_data = listo_o_firma_schema.load(sol_json)
        
        if check(sol_data['correoDoc']):
            resultado = sendEmailFirma(**sol_data)
            if resultado:
                return {'message': 'Envío exitoso'}
            return {'error': 'Fallo de envío de correo'}
        return {'error': 'No es un correo'}, 400