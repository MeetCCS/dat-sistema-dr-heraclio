''' Solution from https://www.geeksforgeeks.org/check-if-email-address-valid-or-not-in-python/ '''
import re

# for validating an Email
# regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
# para correos personalizados hasta un .com.mx:
regex = '^[a-zA-Z0-9]+[\._-]?[a-zA-Z0-9]+[@]\w+[.]\w{1,}[.]?[a-zA-Z]+?$' #creacion 1
# regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,}[.]?\w{2,}$' #creacion 2


def check(email):
    # pass the regular expression
    # and the string in search() method
    if(re.search(regex, email)):
        return True

    return False
